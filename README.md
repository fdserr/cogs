# cogs


## Hack it


### Platform

- JDK 1.8
- Clojure 1.10
- Docker 18


### REPL

```
$ cd cogs
$ clj -Arepl
```
`user` ns loads CTNR and `clojure.test`.

See `repl_sessions/` for examples.


### Testing

```
$ cd cogs
$ clj -Atest
```

See `test/` for examples.

Note: Clojure 1.10 allows to write tests anywhere.
See "OMITTING TESTS FROM PRODUCTION CODE" on the official `clojure.test`
documentation: https://clojure.github.io/clojure/#clojure.test.


### Merge Requests

Pre-req: merge request opened on GitLab (!1 here).

```
$ git clone https://gitlab.com/fdserr/cogs.git
$ cd cogs
$ git fetch origin
$ git checkout origin/merge-requests/1
```
