#!/bin/bash

# Build the cogs builder image and push it to DockerHub
# IMPORTANT: Always increase TAG before pushing changed image to dockerhub
GRAALVM_IMAGE_TAG=oracle/graalvm-ce:1.0.0-rc12
IMAGE_NAME=fdserr/cogs-builder
TAG=0.0.1

# Build
docker build --tag ${IMAGE_NAME}:${TAG} ./docker/builder/.

if [ $? ]
then
  # Push
  if docker login; then
    docker push ${IMAGE_NAME}:${TAG}
  fi
fi

exit $?
