(ns cogs.main
  (:require
   [clojure.test :as test]
   cogs.core-test))

(defn -main
  [& args]
  (test/run-tests 'cogs.core-test))
