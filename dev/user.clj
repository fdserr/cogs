(ns user
  (:require
   [clojure.java.shell :refer [sh]]
   [clojure.pprint :refer [pprint]]
   [clojure.test :refer [run-tests run-all-tests]]
   [clojure.tools.namespace.repl :refer [refresh set-refresh-dirs]]))
