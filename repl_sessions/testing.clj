;;; NOT A NS

(set-refresh-dirs "src" "test")

(require
 [cogs.core :refer :all]
 'cogs.core-test)

(refresh)

(run-tests 'cogs.core-test)
